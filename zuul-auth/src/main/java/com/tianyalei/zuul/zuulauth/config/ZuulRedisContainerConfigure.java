package com.tianyalei.zuul.zuulauth.config;


import com.tianyalei.core.zuulauth.config.RedisContainerConfigure;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.cloud.netflix.zuul.ZuulProxyMarkerConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;


/**
 * @author wuweifeng wrote on 2019/8/12.
 *
 */
@Configuration
@ConditionalOnMissingBean(RedisMessageListenerContainer.class)
@ConditionalOnBean(ZuulProxyMarkerConfiguration.class) //这一句是当前工程是zuul工程时，才启用该configuration
public class ZuulRedisContainerConfigure extends RedisContainerConfigure {

}
